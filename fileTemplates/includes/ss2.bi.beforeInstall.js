    /**
     * <code>beforeInstall</code> event handler
     * 
     * @governance XXX
     * 
     * @param params
     * 		{Object}            
     * @param params.version
     * 		{Number} The version of the bundle that is being installed
     * 
     * @return {void}
     * 
     * @static
     * @function beforeInstall
     */
    function beforeInstall(params) {
        // TODO
    }
