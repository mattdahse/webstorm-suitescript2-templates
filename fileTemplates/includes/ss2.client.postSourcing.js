    /**
     * <code>postSourcing</code> event handler
     * 
     * @governance XXX
     * 
     * @param context
	 * 		{Object}            
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{String} The internal ID of the sublist.
     * @param context.fieldId
	 * 		{String} The internal ID of the field that triggered
     *            <code>postSourcing</code>.
     * 
     * @return {void}
     * 
     * @static
     * @function postSourcing
     */
    function postSourcing(context) {
    	// TODO
    }
