    /**
     * <code>onRequest</code> event handler
     * 
     * @governance XXX
     *
     * @param context
	 * 		{Object}            
     * @param context.request
	 * 		{ServerRequest} The incoming request object
     * @param context.response
	 * 		{ServerResponse} The outgoing response object
     * 
     * @return {void}
     * 
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        // TODO
    }
