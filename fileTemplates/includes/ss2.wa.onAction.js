    /**
     * <code>onAction</code> event handler
     * 
     * @governance XXX
     *
     * @param context
	 * 		{Object}
     * @param context.newRecord
	 *		{Record} The new record with all changes. <code>save()</code> is not
	 *		permitted.
     * @param context.oldRecord
	 * 		{Record} The old record with all changes. <code>save()</code> is not
	 * 		permitted.
     * 
     * @return {void}
     * 
     * @static
     * @function onAction
     */
    function onAction(context) {
        // TODO
    }
