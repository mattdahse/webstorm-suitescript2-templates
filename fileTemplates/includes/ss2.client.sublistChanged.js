    /**
     * <code>sublistChanged</code> event handler
     * 
     * @governance XXX
     * 
     * @param context
	 * 		{Object}
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{String} The internal ID of the sublist.
     * 
     * @return {void}
     * 
     * @static
     * @function sublistChanged
     */
    function sublistChanged(context) {
    	// TODO
    }
