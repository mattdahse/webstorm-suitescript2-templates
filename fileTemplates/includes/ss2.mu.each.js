    /**
     * <code>each</code> event handler
     * 
     * @governance XXX
     *
     * @param params
	 * 		{Object}
     * @param params.id
	 * 		{Number} The internal ID of the record being processed
     * @param params.type
	 * 		{String} The record type of the record being processed
     * 
     * @return {void}
     * 
     * @static
     * @function each
     */
    function each(params) {
        // TODO
    }
