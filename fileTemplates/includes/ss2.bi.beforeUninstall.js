    /**
     * <code>beforeUninstall</code> event handler
     * 
     * @governance XXX
     * 
     * @param params
     * 		{Object}
     * @param params.version
     * 		{Number} The version of the bundle that is being uninstalled
     * 
     * @return {void}
     * 
     * @static
     * @function beforeUninstall
     */
    function beforeUninstall(params) {
        // TODO
    }
