    /**
     * <code>validateInsert</code> event handler
     * 
     * @governance XXX
     * 
     * @param context
	 * 		{Object}
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{String} The internal ID of the sublist.
     * 
     * @return {Boolean} <code>true</code> if the line can be inserted;
     *         <code>false</code> to prevent the line insertion.
     * 
     * @static
     * @function validateInsert
     */
    function validateInsert(context) {
    	// TODO
    }
