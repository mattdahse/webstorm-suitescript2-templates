    /**
     * <code>beforeLoad</code> event handler
     * for details.
     * 
     * @governance XXX
     * 
     * @param context
     * 		{Object}
     * @param context.newRecord
	 * 		{record} The new record being loaded
     * @param context.type
	 * 		{UserEventType} The action type that triggered this event
     * @param context.form
	 * 		{form} The current UI form
     * 
     * @return {void}
     * 
     * @static
     * @function beforeLoad
     */
    function beforeLoad(context) {
        // TODO
    }
